package co.edu.javeriena.dto;

import java.util.Date;

public class MovimientoDTO {

	private int idMovimiento;

	private String descripcionMovieminto;

	private Date fechaMovieminto;

	private String oficinaMovieminto;

	private String referenciaMovieminto;

	private String valorMovieminto;

	public int getIdMovimiento() {
		return idMovimiento;
	}

	public void setIdMovimiento(int idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public String getDescripcionMovieminto() {
		return descripcionMovieminto;
	}

	public void setDescripcionMovieminto(String descripcionMovieminto) {
		this.descripcionMovieminto = descripcionMovieminto;
	}

	public Date getFechaMovieminto() {
		return fechaMovieminto;
	}

	public void setFechaMovieminto(Date fechaMovieminto) {
		this.fechaMovieminto = fechaMovieminto;
	}

	public String getOficinaMovieminto() {
		return oficinaMovieminto;
	}

	public void setOficinaMovieminto(String oficinaMovieminto) {
		this.oficinaMovieminto = oficinaMovieminto;
	}

	public String getReferenciaMovieminto() {
		return referenciaMovieminto;
	}

	public void setReferenciaMovieminto(String referenciaMovieminto) {
		this.referenciaMovieminto = referenciaMovieminto;
	}

	public String getValorMovieminto() {
		return valorMovieminto;
	}

	public void setValorMovieminto(String valorMovieminto) {
		this.valorMovieminto = valorMovieminto;
	}

}
