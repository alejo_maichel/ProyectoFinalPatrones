package co.edu.javeriana.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the Movimiento database table.
 * 
 */
@Entity
@NamedQuery(name="Movimiento.findAll", query="SELECT m FROM Movimiento m")
public class Movimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idMovimiento;

	private String descripcionMovieminto;

	@Temporal(TemporalType.DATE)
	private Date fechaMovieminto;

	private String oficinaMovieminto;

	private String referenciaMovieminto;

	@Lob
	private String valorMovieminto;

	//bi-directional many-to-one association to Cuenta
	@ManyToOne
	@JoinColumn(name="idCuenta")
	private Cuenta cuenta;

	public Movimiento() {
	}

	public int getIdMovimiento() {
		return this.idMovimiento;
	}

	public void setIdMovimiento(int idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public String getDescripcionMovieminto() {
		return this.descripcionMovieminto;
	}

	public void setDescripcionMovieminto(String descripcionMovieminto) {
		this.descripcionMovieminto = descripcionMovieminto;
	}

	public Date getFechaMovieminto() {
		return this.fechaMovieminto;
	}

	public void setFechaMovieminto(Date fechaMovieminto) {
		this.fechaMovieminto = fechaMovieminto;
	}

	public String getOficinaMovieminto() {
		return this.oficinaMovieminto;
	}

	public void setOficinaMovieminto(String oficinaMovieminto) {
		this.oficinaMovieminto = oficinaMovieminto;
	}

	public String getReferenciaMovieminto() {
		return this.referenciaMovieminto;
	}

	public void setReferenciaMovieminto(String referenciaMovieminto) {
		this.referenciaMovieminto = referenciaMovieminto;
	}

	public String getValorMovieminto() {
		return this.valorMovieminto;
	}

	public void setValorMovieminto(String valorMovieminto) {
		this.valorMovieminto = valorMovieminto;
	}

	public Cuenta getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

}