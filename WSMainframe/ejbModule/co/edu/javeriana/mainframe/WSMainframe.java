package co.edu.javeriana.mainframe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import co.edu.javeriana.model.Cuenta;
import co.edu.javeriana.model.Movimiento;
import co.edu.javeriena.dto.CuentaDTO;
import co.edu.javeriena.dto.MovimientoDTO;

@Stateless
@WebService
public class WSMainframe {

	@PersistenceContext
	private EntityManager em;

	@WebMethod
	public List<CuentaDTO> consultarCuentasCliente(String idCliente) {
		if (idCliente != null && idCliente.length() >= 6) {
			List<Cuenta> cuentasTemp = em.createNamedQuery("Cuenta.findCuentasCliente").setParameter("idcliente", idCliente).getResultList();
			
			List<CuentaDTO> cuentasCliente = castCuentaToXML(cuentasTemp);
			return cuentasCliente;
		}

		return null;
	}
	
	@WebMethod 
	public CuentaDTO consultarMoviminetosCuenta(Integer idCuenta){
		
		if (idCuenta != null) {
			List<Cuenta> cuentasTemp = em.createNamedQuery("Cuenta.findCuentaCliente").setParameter("idcuenta", idCuenta).getResultList();
			List<CuentaDTO> cuentasCliente = castCuentaToXML(cuentasTemp);
			if(cuentasCliente.size()>0){
				CuentaDTO dto = cuentasCliente.get(0);
				return dto;
			}
		}
		return null;
	}
	
	@WebMethod
	public boolean debitarSaldoCuenta(String idCuenta,Integer valorDebitar){
		
		Cuenta cuentasTemp = (Cuenta) em.createNamedQuery("Cuenta.findNumeroCuenta").setParameter("numerocuenta", idCuenta).getSingleResult();
		Integer saldoCuenta = Integer.parseInt(cuentasTemp.getSaldoCuenta());
		if(saldoCuenta>valorDebitar){
			saldoCuenta=saldoCuenta-valorDebitar;
			cuentasTemp.setSaldoCuenta(saldoCuenta.toString());
			
			Movimiento mov = new Movimiento();
			mov.setCuenta(cuentasTemp);
			mov.setDescripcionMovieminto("PAGO PROGRAMADO");
			mov.setFechaMovieminto(new Date());
			mov.setOficinaMovieminto("VIRTUAL");
			mov.setReferenciaMovieminto("1234567890");
			mov.setValorMovieminto(valorDebitar.toString());
			em.merge(cuentasTemp);
			em.persist(mov);
			return true;
		}
		
		return false;
	}

	private List<CuentaDTO> castCuentaToXML(List<Cuenta> cuentasCliente) {
		
		List<CuentaDTO> cuentasRespuesta = new ArrayList<CuentaDTO>();
		
		for (Cuenta cuenta : cuentasCliente) {
			
			CuentaDTO dto = new CuentaDTO();
			dto.setIdCuenta(cuenta.getIdCuenta());
			dto.setNombreCuenta(cuenta.getNombreCuenta());
			dto.setCliente(cuenta.getCliente());
			dto.setNumeroCuenta(cuenta.getNumeroCuenta());
			dto.setSaldoCuenta(cuenta.getSaldoCuenta());
			dto.setTipoCuenta(cuenta.getTipoCuenta());
			
			List<MovimientoDTO> movimientos = new ArrayList<MovimientoDTO>();
			
			for (Movimiento mov : cuenta.getMovimientos()) {
				MovimientoDTO m = new MovimientoDTO();
				m.setDescripcionMovieminto(mov.getDescripcionMovieminto());
				m.setFechaMovieminto(mov.getFechaMovieminto());
				m.setIdMovimiento(mov.getIdMovimiento());
				m.setOficinaMovieminto(mov.getOficinaMovieminto());
				m.setReferenciaMovieminto(mov.getReferenciaMovieminto());
				m.setValorMovieminto(mov.getValorMovieminto());
				
				movimientos.add(m);
			}
			
			dto.setMovimientos(movimientos);
			
			cuentasRespuesta.add(dto);
		}

		return cuentasRespuesta;
	}
}
