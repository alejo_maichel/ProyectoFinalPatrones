package co.edu.javeriana.pagos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import co.edu.javeriana.model.Factura;
import co.edu.javeriana.model.PagoProgramado;
import co.edu.javeriana.pojo.PagoProgramadoDTO;
import co.edu.javeriana.pojo.PagoProgramadoDTO.tipoPeriodicidad;

@Stateless
@WebService
public class WSPagosProgramados {

	@PersistenceContext
	private EntityManager em;

	@WebMethod
	public boolean agregarNuevoPagoProgramado(PagoProgramadoDTO dto) {
		try {
			if (dto != null) {
				Factura factura = new Factura();
				factura.setNumeroFactura(dto.getNumeroFactura());
				factura.setConvenioFactura(dto.getConvenioFactura());
				factura.setValorFactura(dto.getValorFactura());
				em.persist(factura);

				PagoProgramado pagoProgramado = new PagoProgramado();
				pagoProgramado.setIdentificadorCliente(dto.getIdentificadorCliente());
				pagoProgramado.setCuentaDebitar(dto.getCuentaDebitar());
				pagoProgramado.setFechaRealizacion(dto.getFechaEjecucuion());
				pagoProgramado.setIdentificadorObligacion(dto.getNumeroFactura());
				pagoProgramado.setIdentificadorObligacion(dto.getIdentificacionPago());
				pagoProgramado.setPeriodicidad(dto.getPeriodicidad().toString());
				pagoProgramado.setValorTransaccion(dto.getValorFactura());
				pagoProgramado.setFactura(factura);
				
				em.persist(pagoProgramado);

				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	@WebMethod
	public boolean eliminarNuevoPagoProgramado(Integer identificadorPago) {
		try {
			em.remove(em.find(PagoProgramado.class, identificadorPago));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@WebMethod
	public List<PagoProgramadoDTO> consultarPagosProgramadosCliente(String idCliente) {
		List<PagoProgramadoDTO> resultado = null;
		if (idCliente != null && !idCliente.isEmpty()) {
			List<PagoProgramado> listaPagos = em.createNamedQuery("PagoProgramado.findPagosCliente")
					.setParameter("idcliente", idCliente).getResultList();
			resultado = castPagosToXML(listaPagos);
		}
		return resultado;
	}
	
	@WebMethod
	public List<PagoProgramadoDTO> consultarPagosProgramadosCRON(Date fechaConsulta) {
		List<PagoProgramadoDTO> resultado = null;
		if (fechaConsulta != null) {
			Date fechaInicio = (Date) fechaConsulta.clone();
			fechaInicio.setHours(0);
			fechaInicio.setMinutes(0);
			fechaInicio.setSeconds(0);
			
			System.out.println(fechaInicio.toString());
			
			Date fechaFin = (Date) fechaConsulta.clone();
			fechaFin.setHours(23);
			fechaFin.setMinutes(59);
			fechaFin.setSeconds(59);
			
			System.out.println(fechaFin.toString());
			
			List<PagoProgramado> listaPagos = em.createNamedQuery("PagoProgramado.findPagosCRON")
					.setParameter("fechaInicio", fechaInicio).setParameter("fechaFin", fechaFin).getResultList();
			resultado = castPagosToXML(listaPagos);
		}
		return resultado;
	}

	protected List<PagoProgramadoDTO> castPagosToXML(List<PagoProgramado> listaPagos) {

		List<PagoProgramadoDTO> listaRespuesta = new ArrayList<>();
		for (PagoProgramado pagoProgramado : listaPagos) {
			PagoProgramadoDTO dto = new PagoProgramadoDTO();
			dto.setIdPago(pagoProgramado.getIdPagoProgramado());
			dto.setConvenioFactura(pagoProgramado.getFactura().getConvenioFactura());
			dto.setCuentaDebitar(pagoProgramado.getCuentaDebitar());
			dto.setEmpresaFactura(pagoProgramado.getFactura().getConvenioFactura());
			dto.setFechaEjecucuion(pagoProgramado.getFechaRealizacion());
			dto.setIdentificacionPago(pagoProgramado.getIdentificadorObligacion());
			dto.setIdentificadorCliente(pagoProgramado.getIdentificadorCliente());
			dto.setNumeroFactura(pagoProgramado.getFactura().getNumeroFactura());
			switch (pagoProgramado.getPeriodicidad()) {
			case "ANUAL":
				dto.setPeriodicidad(tipoPeriodicidad.ANUAL);
				break;
			case "MENSUAL":
				dto.setPeriodicidad(tipoPeriodicidad.MENSUAL);
				break;
			case "SEMESTRAL":
				dto.setPeriodicidad(tipoPeriodicidad.SEMESTRAL);
				break;
			case "TRIMESTRAL":
				dto.setPeriodicidad(tipoPeriodicidad.TRIMESTRAL);
				break;
			}
			dto.setValorFactura(pagoProgramado.getValorTransaccion());
			listaRespuesta.add(dto);
		}
		return listaRespuesta;
	}
}
