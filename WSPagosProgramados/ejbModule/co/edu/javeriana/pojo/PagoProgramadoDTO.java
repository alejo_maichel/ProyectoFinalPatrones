package co.edu.javeriana.pojo;

import java.util.Calendar;
import java.util.Date;

public class PagoProgramadoDTO {

	public enum tipoPeriodicidad {
		MENSUAL, TRIMESTRAL, SEMESTRAL, ANUAL
	}

	private Integer idPago;
	private String empresaFactura;
	private String convenioFactura;
	private String numeroFactura;
	private String valorFactura;

	private Date fechaEjecucuion;
	private String cuentaDebitar;
	private String identificacionPago;
	private tipoPeriodicidad periodicidad;
	private String identificadorCliente;

	public PagoProgramadoDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public Date getFechaEjecucuion() {
		return fechaEjecucuion;
	}

	public void setFechaEjecucuion(Date fechaEjecucuion) {
		this.fechaEjecucuion = fechaEjecucuion;
	}

	public String getIdentificadorCliente() {
		return identificadorCliente;
	}

	public void setIdentificadorCliente(String identificadorCliente) {
		this.identificadorCliente = identificadorCliente;
	}

	public String getEmpresaFactura() {
		return empresaFactura;
	}

	public void setEmpresaFactura(String empresaFactura) {
		this.empresaFactura = empresaFactura;
	}

	public String getConvenioFactura() {
		return convenioFactura;
	}

	public void setConvenioFactura(String convenioFactura) {
		this.convenioFactura = convenioFactura;
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public String getValorFactura() {
		return valorFactura;
	}

	public void setValorFactura(String valorFactura) {
		this.valorFactura = valorFactura;
	}

	public Integer getIdPago() {
		return idPago;
	}

	public void setIdPago(Integer idPago) {
		this.idPago = idPago;
	}

	public String getCuentaDebitar() {
		return cuentaDebitar;
	}

	public void setCuentaDebitar(String cuentaDebitar) {
		this.cuentaDebitar = cuentaDebitar;
	}

	public String getIdentificacionPago() {
		return identificacionPago;
	}

	public void setIdentificacionPago(String identificacionPago) {
		this.identificacionPago = identificacionPago;
	}

	public tipoPeriodicidad getPeriodicidad() {
		return periodicidad;
	}

	public void setPeriodicidad(tipoPeriodicidad periodicidad) {
		this.periodicidad = periodicidad;
	}

}
