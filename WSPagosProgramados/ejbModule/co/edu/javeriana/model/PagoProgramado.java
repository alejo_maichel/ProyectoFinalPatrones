package co.edu.javeriana.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the PagoProgramado database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "PagoProgramado.findAll", query = "SELECT p FROM PagoProgramado p"),
	@NamedQuery(name = "PagoProgramado.findPagosCliente", query = "SELECT p FROM PagoProgramado p where p.identificadorCliente = :idcliente"),
	@NamedQuery(name = "PagoProgramado.findPagosCRON", query = "SELECT p FROM PagoProgramado p where p.fechaRealizacion between :fechaInicio and :fechaFin")
})
public class PagoProgramado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPagoProgramado;

	private String cuentaDebitar;

	@Temporal(TemporalType.DATE)
	private Date fechaRealizacion;

	private String identificadorObligacion;

	private String periodicidad;

	private String identificadorCliente;

	@Lob
	private String valorTransaccion;

	// bi-directional many-to-one association to Factura
	@ManyToOne
	@JoinColumn(name = "idFacturas")
	private Factura factura;

	// bi-directional many-to-one association to PagoRealizado
	@OneToMany(mappedBy = "pagoProgramado")
	private List<PagoRealizado> pagoRealizados;

	public PagoProgramado() {
	}

	public int getIdPagoProgramado() {
		return this.idPagoProgramado;
	}

	public void setIdPagoProgramado(int idPagoProgramado) {
		this.idPagoProgramado = idPagoProgramado;
	}

	public String getCuentaDebitar() {
		return this.cuentaDebitar;
	}

	public void setCuentaDebitar(String cuentaDebitar) {
		this.cuentaDebitar = cuentaDebitar;
	}

	public Date getFechaRealizacion() {
		return this.fechaRealizacion;
	}

	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}

	public String getIdentificadorObligacion() {
		return this.identificadorObligacion;
	}

	public void setIdentificadorObligacion(String identificadorObligacion) {
		this.identificadorObligacion = identificadorObligacion;
	}

	public String getPeriodicidad() {
		return this.periodicidad;
	}

	public void setPeriodicidad(String periodicidad) {
		this.periodicidad = periodicidad;
	}

	public String getValorTransaccion() {
		return this.valorTransaccion;
	}

	public void setValorTransaccion(String valorTransaccion) {
		this.valorTransaccion = valorTransaccion;
	}

	public Factura getFactura() {
		return this.factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public List<PagoRealizado> getPagoRealizados() {
		return this.pagoRealizados;
	}

	public void setPagoRealizados(List<PagoRealizado> pagoRealizados) {
		this.pagoRealizados = pagoRealizados;
	}

	public String getIdentificadorCliente() {
		return identificadorCliente;
	}

	public void setIdentificadorCliente(String identificadorCliente) {
		this.identificadorCliente = identificadorCliente;
	}

	public PagoRealizado addPagoRealizado(PagoRealizado pagoRealizado) {
		getPagoRealizados().add(pagoRealizado);
		pagoRealizado.setPagoProgramado(this);

		return pagoRealizado;
	}

	public PagoRealizado removePagoRealizado(PagoRealizado pagoRealizado) {
		getPagoRealizados().remove(pagoRealizado);
		pagoRealizado.setPagoProgramado(null);

		return pagoRealizado;
	}

}