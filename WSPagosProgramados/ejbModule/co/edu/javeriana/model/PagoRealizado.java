package co.edu.javeriana.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PagoRealizado database table.
 * 
 */
@Entity
@NamedQuery(name="PagoRealizado.findAll", query="SELECT p FROM PagoRealizado p")
public class PagoRealizado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPagoRealizado;

	@Temporal(TemporalType.DATE)
	private Date fechaPago;

	private String numeroReferencia;

	//bi-directional many-to-one association to PagoProgramado
	@ManyToOne
	@JoinColumn(name="PagoProgramado_idPagoProgramado")
	private PagoProgramado pagoProgramado;

	public PagoRealizado() {
	}

	public int getIdPagoRealizado() {
		return this.idPagoRealizado;
	}

	public void setIdPagoRealizado(int idPagoRealizado) {
		this.idPagoRealizado = idPagoRealizado;
	}

	public Date getFechaPago() {
		return this.fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getNumeroReferencia() {
		return this.numeroReferencia;
	}

	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

	public PagoProgramado getPagoProgramado() {
		return this.pagoProgramado;
	}

	public void setPagoProgramado(PagoProgramado pagoProgramado) {
		this.pagoProgramado = pagoProgramado;
	}

}