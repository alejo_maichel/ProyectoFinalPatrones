package co.edu.javeriana.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Factura database table.
 * 
 */
@Entity
@NamedQuery(name="Factura.findAll", query="SELECT f FROM Factura f")
public class Factura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idFacturas;

	private String numeroFactura;
	
	@Lob
	private String convenioFactura;

	private String valorFactura;

	//bi-directional many-to-one association to PagoProgramado
	@OneToMany(mappedBy="factura")
	private List<PagoProgramado> pagoProgramados;

	public Factura() {
	}

	public int getIdFacturas() {
		return this.idFacturas;
	}

	public void setIdFacturas(int idFacturas) {
		this.idFacturas = idFacturas;
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public String getConvenioFactura() {
		return this.convenioFactura;
	}

	public void setConvenioFactura(String convenioFactura) {
		this.convenioFactura = convenioFactura;
	}

	public String getValorFactura() {
		return this.valorFactura;
	}

	public void setValorFactura(String valorFactura) {
		this.valorFactura = valorFactura;
	}

	public List<PagoProgramado> getPagoProgramados() {
		return this.pagoProgramados;
	}

	public void setPagoProgramados(List<PagoProgramado> pagoProgramados) {
		this.pagoProgramados = pagoProgramados;
	}

	public PagoProgramado addPagoProgramado(PagoProgramado pagoProgramado) {
		getPagoProgramados().add(pagoProgramado);
		pagoProgramado.setFactura(this);

		return pagoProgramado;
	}

	public PagoProgramado removePagoProgramado(PagoProgramado pagoProgramado) {
		getPagoProgramados().remove(pagoProgramado);
		pagoProgramado.setFactura(null);

		return pagoProgramado;
	}

}