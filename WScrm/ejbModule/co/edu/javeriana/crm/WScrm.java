package co.edu.javeriana.crm;

import java.util.Date;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import co.edu.javeriana.pojo.Cliente;

@Stateless
@WebService
public class WScrm {

	@WebMethod
	public Cliente consultarClienteCRM(String idCliente) {

		Cliente clienteRespuesta = null;
		if (idCliente != null) {
			switch (idCliente) {
			case "1200001":
				clienteRespuesta = new Cliente();
				clienteRespuesta.setId("1200001");
				clienteRespuesta.setNombres("Linda");
				clienteRespuesta.setApellidos("Polania");
				clienteRespuesta.setFechaNacimiento(new Date());
				clienteRespuesta.setDireccion("CLL 170 # 50 -13");
				clienteRespuesta.setTelefono("3212333");
				clienteRespuesta.setCorreoElectronico("lpolania@gmail.com");
				break;

			case "1200002":
				clienteRespuesta = new Cliente();
				clienteRespuesta.setId("1200002");
				clienteRespuesta.setNombres("Carlos");
				clienteRespuesta.setApellidos("Marin");
				clienteRespuesta.setFechaNacimiento(new Date());
				clienteRespuesta.setDireccion("CLL 170 # 50 -13");
				clienteRespuesta.setTelefono("3212333");
				clienteRespuesta.setCorreoElectronico("cmarin@gmail.com");
				break;

			case "1200003":
				clienteRespuesta = new Cliente();
				clienteRespuesta.setId("1200003");
				clienteRespuesta.setNombres("Arguello");
				clienteRespuesta.setApellidos("Guarnizo");
				clienteRespuesta.setFechaNacimiento(new Date());
				clienteRespuesta.setDireccion("CLL 170 # 50 -13");
				clienteRespuesta.setTelefono("3212333");
				clienteRespuesta.setCorreoElectronico("aguarnizo@gmail.com");
				break;

			case "1200004":
				clienteRespuesta = new Cliente();
				clienteRespuesta.setId("1200004");
				clienteRespuesta.setNombres("Francisco");
				clienteRespuesta.setApellidos("Arguello");
				clienteRespuesta.setFechaNacimiento(new Date());
				clienteRespuesta.setDireccion("CLL 170 # 50 -13");
				clienteRespuesta.setTelefono("3212333");
				clienteRespuesta.setCorreoElectronico("farguello@gmail.com");
				break;

			default:
				break;
			}
		}
		return clienteRespuesta;
	}
}
