package co.edu.javeriana.ad;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import co.edu.javeriana.pojo.Autenticacion;
import co.edu.javeriana.pojo.Usuario;

@Stateless
@WebService
public class wsDirectorioActivo {

	@WebMethod
	public Usuario ConsultarIdentificacion(Autenticacion auth) {

		Usuario usuarioRespuesta = null;
		
		if (auth != null) {
			if (auth.getUsuario() != null && auth.getContrasena() != null) {				
				switch (auth.getUsuario()) {
				case "usuario1":
					usuarioRespuesta = new Usuario();
					usuarioRespuesta.setNombre("Linda");
					usuarioRespuesta.setApellido("Polania");
					usuarioRespuesta.setUsuario("usuario1");
					usuarioRespuesta.setId("1200001");
					break;
					
				case "usuario2":
					usuarioRespuesta = new Usuario();
					usuarioRespuesta.setNombre("Carlos");
					usuarioRespuesta.setApellido("Marin");
					usuarioRespuesta.setUsuario("usuario2");
					usuarioRespuesta.setId("1200002");
					break;
					
				case "usuario3":
					usuarioRespuesta = new Usuario();
					usuarioRespuesta.setNombre("Anthony");
					usuarioRespuesta.setApellido("Guarnizo");
					usuarioRespuesta.setUsuario("usuario3");
					usuarioRespuesta.setId("1200003");
					break;
					
				case "usuario4":
					usuarioRespuesta = new Usuario();
					usuarioRespuesta.setNombre("Francisco");
					usuarioRespuesta.setApellido("Arguello");
					usuarioRespuesta.setUsuario("usuario4");
					usuarioRespuesta.setId("1200004");
					break;

				default:
					break;
				}
			}
		}

		return usuarioRespuesta;
	}
}
