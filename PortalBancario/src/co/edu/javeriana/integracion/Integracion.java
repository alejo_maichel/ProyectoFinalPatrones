package co.edu.javeriana.integracion;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import co.edu.javeriana.integracion.ad.Autenticacion;
import co.edu.javeriana.integracion.ad.Usuario;
import co.edu.javeriana.integracion.ad.WsDirectorioActivo;
import co.edu.javeriana.integracion.ad.WsDirectorioActivoService;
import co.edu.javeriana.integracion.crm.Cliente;
import co.edu.javeriana.integracion.crm.WScrm;
import co.edu.javeriana.integracion.crm.WScrmService;
import co.edu.javeriana.integracion.mainframe.CuentaDTO;
import co.edu.javeriana.integracion.mainframe.WSMainframe;
import co.edu.javeriana.integracion.mainframe.WSMainframeService;
import co.edu.javeriana.integracion.pagoprogramado.PagoProgramadoDTO;
import co.edu.javeriana.integracion.pagoprogramado.WSPagosProgramados;
import co.edu.javeriana.integracion.pagoprogramado.WSPagosProgramadosService;
import co.edu.javeriana.integracion.pasarela.WSpasarela;
import co.edu.javeriana.integracion.pasarela.WSpasarelaService;
import co.edu.javeriana.integracion.prestamos.PrestamoDTO;
import co.edu.javeriana.integracion.prestamos.WSPrestamos;
import co.edu.javeriana.integracion.prestamos.WSPrestamosService;

public class Integracion {

	public Usuario consultarDirectorioActivo(String usuario, String contrasena) {
		Autenticacion auth = new Autenticacion();
		auth.setUsuario(usuario);
		auth.setContrasena(contrasena);

		WsDirectorioActivoService service1 = new WsDirectorioActivoService();
		WsDirectorioActivo port1 = service1.getWsDirectorioActivoPort();
		Usuario usuarioAutenticacion = port1.consultarIdentificacion(auth);

		return usuarioAutenticacion;
	}

	public Cliente consultarClienteCRM(String idCliente) {

		WScrmService service1 = new WScrmService();
		WScrm port1 = service1.getWScrmPort();
		Cliente cliente = port1.consultarClienteCRM(idCliente);
		return cliente;
	}

	public List<CuentaDTO> ConsultaCuentas(String idCliente) {
		WSMainframeService service1 = new WSMainframeService();
		WSMainframe port1 = service1.getWSMainframePort();
		List<CuentaDTO> cuentas = port1.consultarCuentasCliente(idCliente);
		return cuentas;
	}

	public CuentaDTO ConsultarMovimientosCuentas(Integer idcuenta) {
		WSMainframeService service1 = new WSMainframeService();
		WSMainframe port1 = service1.getWSMainframePort();
		CuentaDTO cuentas = port1.consultarMoviminetosCuenta(idcuenta);
		return cuentas;
	}

	public List<PrestamoDTO> ConsultarPrestamos(String idCliente) {
		WSPrestamosService service1 = new WSPrestamosService();
		WSPrestamos port1 = service1.getWSPrestamosPort();
		List<PrestamoDTO> prestamos = port1.consultarPrestamosCliente(idCliente);
		return prestamos;
	}

	public PrestamoDTO ConsultarMovimientosPrestamo(Integer idprestamo) {
		WSPrestamosService service1 = new WSPrestamosService();
		WSPrestamos port1 = service1.getWSPrestamosPort();
		PrestamoDTO prestamos = port1.consultarMoviminetosCuenta(idprestamo);
		return prestamos;
	}

	public List<PagoProgramadoDTO> ConsultarPagosProgramados(String idCliente) {
		WSPagosProgramadosService service1 = new WSPagosProgramadosService();
		WSPagosProgramados port1 = service1.getWSPagosProgramadosPort();
		List<PagoProgramadoDTO> listaPagosCliente = port1.consultarPagosProgramadosCliente(idCliente);
		return listaPagosCliente;
	}

	public void AgregarPagosProgramados(PagoProgramadoDTO nuevoPago) {
		WSPagosProgramadosService service1 = new WSPagosProgramadosService();
		WSPagosProgramados port1 = service1.getWSPagosProgramadosPort();
		port1.agregarNuevoPagoProgramado(nuevoPago);
	}

	public void EliminarPagosProgramados(PagoProgramadoDTO nuevoPago) {
		WSPagosProgramadosService service1 = new WSPagosProgramadosService();
		WSPagosProgramados port1 = service1.getWSPagosProgramadosPort();
		port1.eliminarNuevoPagoProgramado(nuevoPago.getIdPago());
	}

	public boolean debitarCuenta(String idCuenta,Integer valorDebitar) {
		WSMainframeService service1 = new WSMainframeService();
		WSMainframe port1 = service1.getWSMainframePort();
		if(port1.debitarSaldoCuenta(idCuenta,valorDebitar)){
			return true;
		}else{
			return false;
		}
	}

	public void gatewayPasarela(String convenio) {
		WSpasarelaService service1 = new WSpasarelaService();
		WSpasarela port1 = service1.getWSpasarelaPort();

		switch (convenio) {
		case "123":
			port1.pagarCamaraCompensacion();
			break;
		case "456":
			port1.pagarCasaCumpliminetoManual();
			break;
		case "789":
			port1.pagarCuentaCuenta();
			break;
		default:
			port1.pagarSWIFT();
			break;
		}
	}
	
	public List<PagoProgramadoDTO> ConsultarPagosProgramados(Date fechaConsulta) {
		WSPagosProgramadosService service1 = new WSPagosProgramadosService();
		WSPagosProgramados port1 = service1.getWSPagosProgramadosPort();
		List<PagoProgramadoDTO> listaPagosCliente = port1.consultarPagosProgramadosCRON(getXmlGregorianCalendarFromDate(fechaConsulta));
		return listaPagosCliente;
	}
	
	public static XMLGregorianCalendar getXmlGregorianCalendarFromDate(final Date date) {
		try {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}
}
