
package co.edu.javeriana.integracion.crm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.edu.javeriana.integracion.crm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultarClienteCRM_QNAME = new QName("http://crm.javeriana.edu.co/", "consultarClienteCRM");
    private final static QName _ConsultarClienteCRMResponse_QNAME = new QName("http://crm.javeriana.edu.co/", "consultarClienteCRMResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.edu.javeriana.integracion.crm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultarClienteCRM }
     * 
     */
    public ConsultarClienteCRM createConsultarClienteCRM() {
        return new ConsultarClienteCRM();
    }

    /**
     * Create an instance of {@link ConsultarClienteCRMResponse }
     * 
     */
    public ConsultarClienteCRMResponse createConsultarClienteCRMResponse() {
        return new ConsultarClienteCRMResponse();
    }

    /**
     * Create an instance of {@link Cliente }
     * 
     */
    public Cliente createCliente() {
        return new Cliente();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarClienteCRM }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://crm.javeriana.edu.co/", name = "consultarClienteCRM")
    public JAXBElement<ConsultarClienteCRM> createConsultarClienteCRM(ConsultarClienteCRM value) {
        return new JAXBElement<ConsultarClienteCRM>(_ConsultarClienteCRM_QNAME, ConsultarClienteCRM.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarClienteCRMResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://crm.javeriana.edu.co/", name = "consultarClienteCRMResponse")
    public JAXBElement<ConsultarClienteCRMResponse> createConsultarClienteCRMResponse(ConsultarClienteCRMResponse value) {
        return new JAXBElement<ConsultarClienteCRMResponse>(_ConsultarClienteCRMResponse_QNAME, ConsultarClienteCRMResponse.class, null, value);
    }

}
