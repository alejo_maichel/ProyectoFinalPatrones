package co.edu.javeriana.integracion.pasarela;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.1.6
 * 2017-05-28T22:24:04.528-05:00
 * Generated source version: 3.1.6
 * 
 */
@WebServiceClient(name = "WSpasarelaService", 
                  wsdlLocation = "http://localhost:8090/WSPasarela/WSpasarela?wsdl",
                  targetNamespace = "http://pasarela.javeriana.edu.co/") 
public class WSpasarelaService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://pasarela.javeriana.edu.co/", "WSpasarelaService");
    public final static QName WSpasarelaPort = new QName("http://pasarela.javeriana.edu.co/", "WSpasarelaPort");
    static {
        URL url = null;
        try {
            url = new URL("http://localhost:8090/WSPasarela/WSpasarela?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(WSpasarelaService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://localhost:8090/WSPasarela/WSpasarela?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public WSpasarelaService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public WSpasarelaService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public WSpasarelaService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public WSpasarelaService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public WSpasarelaService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public WSpasarelaService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    




    /**
     *
     * @return
     *     returns WSpasarela
     */
    @WebEndpoint(name = "WSpasarelaPort")
    public WSpasarela getWSpasarelaPort() {
        return super.getPort(WSpasarelaPort, WSpasarela.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns WSpasarela
     */
    @WebEndpoint(name = "WSpasarelaPort")
    public WSpasarela getWSpasarelaPort(WebServiceFeature... features) {
        return super.getPort(WSpasarelaPort, WSpasarela.class, features);
    }

}
