
package co.edu.javeriana.integracion.pasarela;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.edu.javeriana.integracion.pasarela package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PagarCamaraCompensacion_QNAME = new QName("http://pasarela.javeriana.edu.co/", "PagarCamaraCompensacion");
    private final static QName _PagarCamaraCompensacionResponse_QNAME = new QName("http://pasarela.javeriana.edu.co/", "PagarCamaraCompensacionResponse");
    private final static QName _PagarCasaCumpliminetoManual_QNAME = new QName("http://pasarela.javeriana.edu.co/", "PagarCasaCumpliminetoManual");
    private final static QName _PagarCasaCumpliminetoManualResponse_QNAME = new QName("http://pasarela.javeriana.edu.co/", "PagarCasaCumpliminetoManualResponse");
    private final static QName _PagarCuentaCuenta_QNAME = new QName("http://pasarela.javeriana.edu.co/", "PagarCuentaCuenta");
    private final static QName _PagarCuentaCuentaResponse_QNAME = new QName("http://pasarela.javeriana.edu.co/", "PagarCuentaCuentaResponse");
    private final static QName _PagarSWIFT_QNAME = new QName("http://pasarela.javeriana.edu.co/", "PagarSWIFT");
    private final static QName _PagarSWIFTResponse_QNAME = new QName("http://pasarela.javeriana.edu.co/", "PagarSWIFTResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.edu.javeriana.integracion.pasarela
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PagarCamaraCompensacion }
     * 
     */
    public PagarCamaraCompensacion createPagarCamaraCompensacion() {
        return new PagarCamaraCompensacion();
    }

    /**
     * Create an instance of {@link PagarCamaraCompensacionResponse }
     * 
     */
    public PagarCamaraCompensacionResponse createPagarCamaraCompensacionResponse() {
        return new PagarCamaraCompensacionResponse();
    }

    /**
     * Create an instance of {@link PagarCasaCumpliminetoManual }
     * 
     */
    public PagarCasaCumpliminetoManual createPagarCasaCumpliminetoManual() {
        return new PagarCasaCumpliminetoManual();
    }

    /**
     * Create an instance of {@link PagarCasaCumpliminetoManualResponse }
     * 
     */
    public PagarCasaCumpliminetoManualResponse createPagarCasaCumpliminetoManualResponse() {
        return new PagarCasaCumpliminetoManualResponse();
    }

    /**
     * Create an instance of {@link PagarCuentaCuenta }
     * 
     */
    public PagarCuentaCuenta createPagarCuentaCuenta() {
        return new PagarCuentaCuenta();
    }

    /**
     * Create an instance of {@link PagarCuentaCuentaResponse }
     * 
     */
    public PagarCuentaCuentaResponse createPagarCuentaCuentaResponse() {
        return new PagarCuentaCuentaResponse();
    }

    /**
     * Create an instance of {@link PagarSWIFT }
     * 
     */
    public PagarSWIFT createPagarSWIFT() {
        return new PagarSWIFT();
    }

    /**
     * Create an instance of {@link PagarSWIFTResponse }
     * 
     */
    public PagarSWIFTResponse createPagarSWIFTResponse() {
        return new PagarSWIFTResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarCamaraCompensacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pasarela.javeriana.edu.co/", name = "PagarCamaraCompensacion")
    public JAXBElement<PagarCamaraCompensacion> createPagarCamaraCompensacion(PagarCamaraCompensacion value) {
        return new JAXBElement<PagarCamaraCompensacion>(_PagarCamaraCompensacion_QNAME, PagarCamaraCompensacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarCamaraCompensacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pasarela.javeriana.edu.co/", name = "PagarCamaraCompensacionResponse")
    public JAXBElement<PagarCamaraCompensacionResponse> createPagarCamaraCompensacionResponse(PagarCamaraCompensacionResponse value) {
        return new JAXBElement<PagarCamaraCompensacionResponse>(_PagarCamaraCompensacionResponse_QNAME, PagarCamaraCompensacionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarCasaCumpliminetoManual }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pasarela.javeriana.edu.co/", name = "PagarCasaCumpliminetoManual")
    public JAXBElement<PagarCasaCumpliminetoManual> createPagarCasaCumpliminetoManual(PagarCasaCumpliminetoManual value) {
        return new JAXBElement<PagarCasaCumpliminetoManual>(_PagarCasaCumpliminetoManual_QNAME, PagarCasaCumpliminetoManual.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarCasaCumpliminetoManualResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pasarela.javeriana.edu.co/", name = "PagarCasaCumpliminetoManualResponse")
    public JAXBElement<PagarCasaCumpliminetoManualResponse> createPagarCasaCumpliminetoManualResponse(PagarCasaCumpliminetoManualResponse value) {
        return new JAXBElement<PagarCasaCumpliminetoManualResponse>(_PagarCasaCumpliminetoManualResponse_QNAME, PagarCasaCumpliminetoManualResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarCuentaCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pasarela.javeriana.edu.co/", name = "PagarCuentaCuenta")
    public JAXBElement<PagarCuentaCuenta> createPagarCuentaCuenta(PagarCuentaCuenta value) {
        return new JAXBElement<PagarCuentaCuenta>(_PagarCuentaCuenta_QNAME, PagarCuentaCuenta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarCuentaCuentaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pasarela.javeriana.edu.co/", name = "PagarCuentaCuentaResponse")
    public JAXBElement<PagarCuentaCuentaResponse> createPagarCuentaCuentaResponse(PagarCuentaCuentaResponse value) {
        return new JAXBElement<PagarCuentaCuentaResponse>(_PagarCuentaCuentaResponse_QNAME, PagarCuentaCuentaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarSWIFT }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pasarela.javeriana.edu.co/", name = "PagarSWIFT")
    public JAXBElement<PagarSWIFT> createPagarSWIFT(PagarSWIFT value) {
        return new JAXBElement<PagarSWIFT>(_PagarSWIFT_QNAME, PagarSWIFT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarSWIFTResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pasarela.javeriana.edu.co/", name = "PagarSWIFTResponse")
    public JAXBElement<PagarSWIFTResponse> createPagarSWIFTResponse(PagarSWIFTResponse value) {
        return new JAXBElement<PagarSWIFTResponse>(_PagarSWIFTResponse_QNAME, PagarSWIFTResponse.class, null, value);
    }

}
