
package co.edu.javeriana.integracion.prestamos;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prestamoDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prestamoDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cantidadCuotas" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="convenio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idPrestamo" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="listaMovimientos" type="{http://prestamos.javeriana.edu.co/}movimientoDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="valorCuota" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="valorPrestamo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prestamoDTO", propOrder = {
    "cantidadCuotas",
    "cliente",
    "convenio",
    "idPrestamo",
    "listaMovimientos",
    "valorCuota",
    "valorPrestamo"
})
public class PrestamoDTO {

    protected int cantidadCuotas;
    protected String cliente;
    protected String convenio;
    protected int idPrestamo;
    @XmlElement(nillable = true)
    protected List<MovimientoDTO> listaMovimientos;
    protected String valorCuota;
    protected String valorPrestamo;

    /**
     * Gets the value of the cantidadCuotas property.
     * 
     */
    public int getCantidadCuotas() {
        return cantidadCuotas;
    }

    /**
     * Sets the value of the cantidadCuotas property.
     * 
     */
    public void setCantidadCuotas(int value) {
        this.cantidadCuotas = value;
    }

    /**
     * Gets the value of the cliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * Sets the value of the cliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliente(String value) {
        this.cliente = value;
    }

    /**
     * Gets the value of the convenio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConvenio() {
        return convenio;
    }

    /**
     * Sets the value of the convenio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConvenio(String value) {
        this.convenio = value;
    }

    /**
     * Gets the value of the idPrestamo property.
     * 
     */
    public int getIdPrestamo() {
        return idPrestamo;
    }

    /**
     * Sets the value of the idPrestamo property.
     * 
     */
    public void setIdPrestamo(int value) {
        this.idPrestamo = value;
    }

    /**
     * Gets the value of the listaMovimientos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaMovimientos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaMovimientos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MovimientoDTO }
     * 
     * 
     */
    public List<MovimientoDTO> getListaMovimientos() {
        if (listaMovimientos == null) {
            listaMovimientos = new ArrayList<MovimientoDTO>();
        }
        return this.listaMovimientos;
    }

    /**
     * Gets the value of the valorCuota property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorCuota() {
        return valorCuota;
    }

    /**
     * Sets the value of the valorCuota property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorCuota(String value) {
        this.valorCuota = value;
    }

    /**
     * Gets the value of the valorPrestamo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorPrestamo() {
        return valorPrestamo;
    }

    /**
     * Sets the value of the valorPrestamo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorPrestamo(String value) {
        this.valorPrestamo = value;
    }

}
