package co.edu.javeriana.integracion.prestamos;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.1.6
 * 2017-05-21T12:36:25.086-05:00
 * Generated source version: 3.1.6
 * 
 */
@WebService(targetNamespace = "http://prestamos.javeriana.edu.co/", name = "WSPrestamos")
@XmlSeeAlso({ObjectFactory.class})
public interface WSPrestamos {

    @WebMethod
    @RequestWrapper(localName = "consultarPrestamosCliente", targetNamespace = "http://prestamos.javeriana.edu.co/", className = "co.edu.javeriana.integracion.prestamos.ConsultarPrestamosCliente")
    @ResponseWrapper(localName = "consultarPrestamosClienteResponse", targetNamespace = "http://prestamos.javeriana.edu.co/", className = "co.edu.javeriana.integracion.prestamos.ConsultarPrestamosClienteResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<co.edu.javeriana.integracion.prestamos.PrestamoDTO> consultarPrestamosCliente(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0
    );

    @WebMethod
    @RequestWrapper(localName = "consultarMoviminetosCuenta", targetNamespace = "http://prestamos.javeriana.edu.co/", className = "co.edu.javeriana.integracion.prestamos.ConsultarMoviminetosCuenta")
    @ResponseWrapper(localName = "consultarMoviminetosCuentaResponse", targetNamespace = "http://prestamos.javeriana.edu.co/", className = "co.edu.javeriana.integracion.prestamos.ConsultarMoviminetosCuentaResponse")
    @WebResult(name = "return", targetNamespace = "")
    public co.edu.javeriana.integracion.prestamos.PrestamoDTO consultarMoviminetosCuenta(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.Integer arg0
    );
}
