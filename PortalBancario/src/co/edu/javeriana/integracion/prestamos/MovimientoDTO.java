
package co.edu.javeriana.integracion.prestamos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for movimientoDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="movimientoDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fechaCobro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idPagosRealizados" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="valorRecaudado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "movimientoDTO", propOrder = {
    "fechaCobro",
    "idPagosRealizados",
    "valorRecaudado"
})
public class MovimientoDTO {

    protected String fechaCobro;
    protected int idPagosRealizados;
    protected String valorRecaudado;

    /**
     * Gets the value of the fechaCobro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCobro() {
        return fechaCobro;
    }

    /**
     * Sets the value of the fechaCobro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCobro(String value) {
        this.fechaCobro = value;
    }

    /**
     * Gets the value of the idPagosRealizados property.
     * 
     */
    public int getIdPagosRealizados() {
        return idPagosRealizados;
    }

    /**
     * Sets the value of the idPagosRealizados property.
     * 
     */
    public void setIdPagosRealizados(int value) {
        this.idPagosRealizados = value;
    }

    /**
     * Gets the value of the valorRecaudado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorRecaudado() {
        return valorRecaudado;
    }

    /**
     * Sets the value of the valorRecaudado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorRecaudado(String value) {
        this.valorRecaudado = value;
    }

}
