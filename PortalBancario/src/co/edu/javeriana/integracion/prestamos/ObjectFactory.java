
package co.edu.javeriana.integracion.prestamos;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.edu.javeriana.integracion.prestamos package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultarMoviminetosCuenta_QNAME = new QName("http://prestamos.javeriana.edu.co/", "consultarMoviminetosCuenta");
    private final static QName _ConsultarMoviminetosCuentaResponse_QNAME = new QName("http://prestamos.javeriana.edu.co/", "consultarMoviminetosCuentaResponse");
    private final static QName _ConsultarPrestamosCliente_QNAME = new QName("http://prestamos.javeriana.edu.co/", "consultarPrestamosCliente");
    private final static QName _ConsultarPrestamosClienteResponse_QNAME = new QName("http://prestamos.javeriana.edu.co/", "consultarPrestamosClienteResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.edu.javeriana.integracion.prestamos
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultarMoviminetosCuenta }
     * 
     */
    public ConsultarMoviminetosCuenta createConsultarMoviminetosCuenta() {
        return new ConsultarMoviminetosCuenta();
    }

    /**
     * Create an instance of {@link ConsultarMoviminetosCuentaResponse }
     * 
     */
    public ConsultarMoviminetosCuentaResponse createConsultarMoviminetosCuentaResponse() {
        return new ConsultarMoviminetosCuentaResponse();
    }

    /**
     * Create an instance of {@link ConsultarPrestamosCliente }
     * 
     */
    public ConsultarPrestamosCliente createConsultarPrestamosCliente() {
        return new ConsultarPrestamosCliente();
    }

    /**
     * Create an instance of {@link ConsultarPrestamosClienteResponse }
     * 
     */
    public ConsultarPrestamosClienteResponse createConsultarPrestamosClienteResponse() {
        return new ConsultarPrestamosClienteResponse();
    }

    /**
     * Create an instance of {@link PrestamoDTO }
     * 
     */
    public PrestamoDTO createPrestamoDTO() {
        return new PrestamoDTO();
    }

    /**
     * Create an instance of {@link MovimientoDTO }
     * 
     */
    public MovimientoDTO createMovimientoDTO() {
        return new MovimientoDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarMoviminetosCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://prestamos.javeriana.edu.co/", name = "consultarMoviminetosCuenta")
    public JAXBElement<ConsultarMoviminetosCuenta> createConsultarMoviminetosCuenta(ConsultarMoviminetosCuenta value) {
        return new JAXBElement<ConsultarMoviminetosCuenta>(_ConsultarMoviminetosCuenta_QNAME, ConsultarMoviminetosCuenta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarMoviminetosCuentaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://prestamos.javeriana.edu.co/", name = "consultarMoviminetosCuentaResponse")
    public JAXBElement<ConsultarMoviminetosCuentaResponse> createConsultarMoviminetosCuentaResponse(ConsultarMoviminetosCuentaResponse value) {
        return new JAXBElement<ConsultarMoviminetosCuentaResponse>(_ConsultarMoviminetosCuentaResponse_QNAME, ConsultarMoviminetosCuentaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPrestamosCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://prestamos.javeriana.edu.co/", name = "consultarPrestamosCliente")
    public JAXBElement<ConsultarPrestamosCliente> createConsultarPrestamosCliente(ConsultarPrestamosCliente value) {
        return new JAXBElement<ConsultarPrestamosCliente>(_ConsultarPrestamosCliente_QNAME, ConsultarPrestamosCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPrestamosClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://prestamos.javeriana.edu.co/", name = "consultarPrestamosClienteResponse")
    public JAXBElement<ConsultarPrestamosClienteResponse> createConsultarPrestamosClienteResponse(ConsultarPrestamosClienteResponse value) {
        return new JAXBElement<ConsultarPrestamosClienteResponse>(_ConsultarPrestamosClienteResponse_QNAME, ConsultarPrestamosClienteResponse.class, null, value);
    }

}
