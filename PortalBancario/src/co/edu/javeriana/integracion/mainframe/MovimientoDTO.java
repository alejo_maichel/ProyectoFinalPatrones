
package co.edu.javeriana.integracion.mainframe;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for movimientoDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="movimientoDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="descripcionMovieminto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaMovieminto" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="idMovimiento" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="oficinaMovieminto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="referenciaMovieminto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="valorMovieminto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "movimientoDTO", propOrder = {
    "descripcionMovieminto",
    "fechaMovieminto",
    "idMovimiento",
    "oficinaMovieminto",
    "referenciaMovieminto",
    "valorMovieminto"
})
public class MovimientoDTO {

    protected String descripcionMovieminto;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaMovieminto;
    protected int idMovimiento;
    protected String oficinaMovieminto;
    protected String referenciaMovieminto;
    protected String valorMovieminto;

    /**
     * Gets the value of the descripcionMovieminto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionMovieminto() {
        return descripcionMovieminto;
    }

    /**
     * Sets the value of the descripcionMovieminto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionMovieminto(String value) {
        this.descripcionMovieminto = value;
    }

    /**
     * Gets the value of the fechaMovieminto property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaMovieminto() {
        return fechaMovieminto;
    }

    /**
     * Sets the value of the fechaMovieminto property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaMovieminto(XMLGregorianCalendar value) {
        this.fechaMovieminto = value;
    }

    /**
     * Gets the value of the idMovimiento property.
     * 
     */
    public int getIdMovimiento() {
        return idMovimiento;
    }

    /**
     * Sets the value of the idMovimiento property.
     * 
     */
    public void setIdMovimiento(int value) {
        this.idMovimiento = value;
    }

    /**
     * Gets the value of the oficinaMovieminto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOficinaMovieminto() {
        return oficinaMovieminto;
    }

    /**
     * Sets the value of the oficinaMovieminto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOficinaMovieminto(String value) {
        this.oficinaMovieminto = value;
    }

    /**
     * Gets the value of the referenciaMovieminto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaMovieminto() {
        return referenciaMovieminto;
    }

    /**
     * Sets the value of the referenciaMovieminto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaMovieminto(String value) {
        this.referenciaMovieminto = value;
    }

    /**
     * Gets the value of the valorMovieminto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorMovieminto() {
        return valorMovieminto;
    }

    /**
     * Sets the value of the valorMovieminto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorMovieminto(String value) {
        this.valorMovieminto = value;
    }

}
