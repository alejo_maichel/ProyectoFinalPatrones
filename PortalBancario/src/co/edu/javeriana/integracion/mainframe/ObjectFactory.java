
package co.edu.javeriana.integracion.mainframe;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.edu.javeriana.integracion.mainframe package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultarCuentasCliente_QNAME = new QName("http://mainframe.javeriana.edu.co/", "consultarCuentasCliente");
    private final static QName _ConsultarCuentasClienteResponse_QNAME = new QName("http://mainframe.javeriana.edu.co/", "consultarCuentasClienteResponse");
    private final static QName _ConsultarMoviminetosCuenta_QNAME = new QName("http://mainframe.javeriana.edu.co/", "consultarMoviminetosCuenta");
    private final static QName _ConsultarMoviminetosCuentaResponse_QNAME = new QName("http://mainframe.javeriana.edu.co/", "consultarMoviminetosCuentaResponse");
    private final static QName _DebitarSaldoCuenta_QNAME = new QName("http://mainframe.javeriana.edu.co/", "debitarSaldoCuenta");
    private final static QName _DebitarSaldoCuentaResponse_QNAME = new QName("http://mainframe.javeriana.edu.co/", "debitarSaldoCuentaResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.edu.javeriana.integracion.mainframe
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultarCuentasCliente }
     * 
     */
    public ConsultarCuentasCliente createConsultarCuentasCliente() {
        return new ConsultarCuentasCliente();
    }

    /**
     * Create an instance of {@link ConsultarCuentasClienteResponse }
     * 
     */
    public ConsultarCuentasClienteResponse createConsultarCuentasClienteResponse() {
        return new ConsultarCuentasClienteResponse();
    }

    /**
     * Create an instance of {@link ConsultarMoviminetosCuenta }
     * 
     */
    public ConsultarMoviminetosCuenta createConsultarMoviminetosCuenta() {
        return new ConsultarMoviminetosCuenta();
    }

    /**
     * Create an instance of {@link ConsultarMoviminetosCuentaResponse }
     * 
     */
    public ConsultarMoviminetosCuentaResponse createConsultarMoviminetosCuentaResponse() {
        return new ConsultarMoviminetosCuentaResponse();
    }

    /**
     * Create an instance of {@link DebitarSaldoCuenta }
     * 
     */
    public DebitarSaldoCuenta createDebitarSaldoCuenta() {
        return new DebitarSaldoCuenta();
    }

    /**
     * Create an instance of {@link DebitarSaldoCuentaResponse }
     * 
     */
    public DebitarSaldoCuentaResponse createDebitarSaldoCuentaResponse() {
        return new DebitarSaldoCuentaResponse();
    }

    /**
     * Create an instance of {@link CuentaDTO }
     * 
     */
    public CuentaDTO createCuentaDTO() {
        return new CuentaDTO();
    }

    /**
     * Create an instance of {@link MovimientoDTO }
     * 
     */
    public MovimientoDTO createMovimientoDTO() {
        return new MovimientoDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarCuentasCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mainframe.javeriana.edu.co/", name = "consultarCuentasCliente")
    public JAXBElement<ConsultarCuentasCliente> createConsultarCuentasCliente(ConsultarCuentasCliente value) {
        return new JAXBElement<ConsultarCuentasCliente>(_ConsultarCuentasCliente_QNAME, ConsultarCuentasCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarCuentasClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mainframe.javeriana.edu.co/", name = "consultarCuentasClienteResponse")
    public JAXBElement<ConsultarCuentasClienteResponse> createConsultarCuentasClienteResponse(ConsultarCuentasClienteResponse value) {
        return new JAXBElement<ConsultarCuentasClienteResponse>(_ConsultarCuentasClienteResponse_QNAME, ConsultarCuentasClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarMoviminetosCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mainframe.javeriana.edu.co/", name = "consultarMoviminetosCuenta")
    public JAXBElement<ConsultarMoviminetosCuenta> createConsultarMoviminetosCuenta(ConsultarMoviminetosCuenta value) {
        return new JAXBElement<ConsultarMoviminetosCuenta>(_ConsultarMoviminetosCuenta_QNAME, ConsultarMoviminetosCuenta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarMoviminetosCuentaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mainframe.javeriana.edu.co/", name = "consultarMoviminetosCuentaResponse")
    public JAXBElement<ConsultarMoviminetosCuentaResponse> createConsultarMoviminetosCuentaResponse(ConsultarMoviminetosCuentaResponse value) {
        return new JAXBElement<ConsultarMoviminetosCuentaResponse>(_ConsultarMoviminetosCuentaResponse_QNAME, ConsultarMoviminetosCuentaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DebitarSaldoCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mainframe.javeriana.edu.co/", name = "debitarSaldoCuenta")
    public JAXBElement<DebitarSaldoCuenta> createDebitarSaldoCuenta(DebitarSaldoCuenta value) {
        return new JAXBElement<DebitarSaldoCuenta>(_DebitarSaldoCuenta_QNAME, DebitarSaldoCuenta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DebitarSaldoCuentaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mainframe.javeriana.edu.co/", name = "debitarSaldoCuentaResponse")
    public JAXBElement<DebitarSaldoCuentaResponse> createDebitarSaldoCuentaResponse(DebitarSaldoCuentaResponse value) {
        return new JAXBElement<DebitarSaldoCuentaResponse>(_DebitarSaldoCuentaResponse_QNAME, DebitarSaldoCuentaResponse.class, null, value);
    }

}
