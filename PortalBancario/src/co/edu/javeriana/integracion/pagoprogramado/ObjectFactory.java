
package co.edu.javeriana.integracion.pagoprogramado;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.edu.javeriana.integracion.pagoprogramado package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AgregarNuevoPagoProgramado_QNAME = new QName("http://pagos.javeriana.edu.co/", "agregarNuevoPagoProgramado");
    private final static QName _AgregarNuevoPagoProgramadoResponse_QNAME = new QName("http://pagos.javeriana.edu.co/", "agregarNuevoPagoProgramadoResponse");
    private final static QName _ConsultarPagosProgramadosCRON_QNAME = new QName("http://pagos.javeriana.edu.co/", "consultarPagosProgramadosCRON");
    private final static QName _ConsultarPagosProgramadosCRONResponse_QNAME = new QName("http://pagos.javeriana.edu.co/", "consultarPagosProgramadosCRONResponse");
    private final static QName _ConsultarPagosProgramadosCliente_QNAME = new QName("http://pagos.javeriana.edu.co/", "consultarPagosProgramadosCliente");
    private final static QName _ConsultarPagosProgramadosClienteResponse_QNAME = new QName("http://pagos.javeriana.edu.co/", "consultarPagosProgramadosClienteResponse");
    private final static QName _EliminarNuevoPagoProgramado_QNAME = new QName("http://pagos.javeriana.edu.co/", "eliminarNuevoPagoProgramado");
    private final static QName _EliminarNuevoPagoProgramadoResponse_QNAME = new QName("http://pagos.javeriana.edu.co/", "eliminarNuevoPagoProgramadoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.edu.javeriana.integracion.pagoprogramado
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AgregarNuevoPagoProgramado }
     * 
     */
    public AgregarNuevoPagoProgramado createAgregarNuevoPagoProgramado() {
        return new AgregarNuevoPagoProgramado();
    }

    /**
     * Create an instance of {@link AgregarNuevoPagoProgramadoResponse }
     * 
     */
    public AgregarNuevoPagoProgramadoResponse createAgregarNuevoPagoProgramadoResponse() {
        return new AgregarNuevoPagoProgramadoResponse();
    }

    /**
     * Create an instance of {@link ConsultarPagosProgramadosCRON }
     * 
     */
    public ConsultarPagosProgramadosCRON createConsultarPagosProgramadosCRON() {
        return new ConsultarPagosProgramadosCRON();
    }

    /**
     * Create an instance of {@link ConsultarPagosProgramadosCRONResponse }
     * 
     */
    public ConsultarPagosProgramadosCRONResponse createConsultarPagosProgramadosCRONResponse() {
        return new ConsultarPagosProgramadosCRONResponse();
    }

    /**
     * Create an instance of {@link ConsultarPagosProgramadosCliente }
     * 
     */
    public ConsultarPagosProgramadosCliente createConsultarPagosProgramadosCliente() {
        return new ConsultarPagosProgramadosCliente();
    }

    /**
     * Create an instance of {@link ConsultarPagosProgramadosClienteResponse }
     * 
     */
    public ConsultarPagosProgramadosClienteResponse createConsultarPagosProgramadosClienteResponse() {
        return new ConsultarPagosProgramadosClienteResponse();
    }

    /**
     * Create an instance of {@link EliminarNuevoPagoProgramado }
     * 
     */
    public EliminarNuevoPagoProgramado createEliminarNuevoPagoProgramado() {
        return new EliminarNuevoPagoProgramado();
    }

    /**
     * Create an instance of {@link EliminarNuevoPagoProgramadoResponse }
     * 
     */
    public EliminarNuevoPagoProgramadoResponse createEliminarNuevoPagoProgramadoResponse() {
        return new EliminarNuevoPagoProgramadoResponse();
    }

    /**
     * Create an instance of {@link PagoProgramadoDTO }
     * 
     */
    public PagoProgramadoDTO createPagoProgramadoDTO() {
        return new PagoProgramadoDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarNuevoPagoProgramado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pagos.javeriana.edu.co/", name = "agregarNuevoPagoProgramado")
    public JAXBElement<AgregarNuevoPagoProgramado> createAgregarNuevoPagoProgramado(AgregarNuevoPagoProgramado value) {
        return new JAXBElement<AgregarNuevoPagoProgramado>(_AgregarNuevoPagoProgramado_QNAME, AgregarNuevoPagoProgramado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarNuevoPagoProgramadoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pagos.javeriana.edu.co/", name = "agregarNuevoPagoProgramadoResponse")
    public JAXBElement<AgregarNuevoPagoProgramadoResponse> createAgregarNuevoPagoProgramadoResponse(AgregarNuevoPagoProgramadoResponse value) {
        return new JAXBElement<AgregarNuevoPagoProgramadoResponse>(_AgregarNuevoPagoProgramadoResponse_QNAME, AgregarNuevoPagoProgramadoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPagosProgramadosCRON }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pagos.javeriana.edu.co/", name = "consultarPagosProgramadosCRON")
    public JAXBElement<ConsultarPagosProgramadosCRON> createConsultarPagosProgramadosCRON(ConsultarPagosProgramadosCRON value) {
        return new JAXBElement<ConsultarPagosProgramadosCRON>(_ConsultarPagosProgramadosCRON_QNAME, ConsultarPagosProgramadosCRON.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPagosProgramadosCRONResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pagos.javeriana.edu.co/", name = "consultarPagosProgramadosCRONResponse")
    public JAXBElement<ConsultarPagosProgramadosCRONResponse> createConsultarPagosProgramadosCRONResponse(ConsultarPagosProgramadosCRONResponse value) {
        return new JAXBElement<ConsultarPagosProgramadosCRONResponse>(_ConsultarPagosProgramadosCRONResponse_QNAME, ConsultarPagosProgramadosCRONResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPagosProgramadosCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pagos.javeriana.edu.co/", name = "consultarPagosProgramadosCliente")
    public JAXBElement<ConsultarPagosProgramadosCliente> createConsultarPagosProgramadosCliente(ConsultarPagosProgramadosCliente value) {
        return new JAXBElement<ConsultarPagosProgramadosCliente>(_ConsultarPagosProgramadosCliente_QNAME, ConsultarPagosProgramadosCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPagosProgramadosClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pagos.javeriana.edu.co/", name = "consultarPagosProgramadosClienteResponse")
    public JAXBElement<ConsultarPagosProgramadosClienteResponse> createConsultarPagosProgramadosClienteResponse(ConsultarPagosProgramadosClienteResponse value) {
        return new JAXBElement<ConsultarPagosProgramadosClienteResponse>(_ConsultarPagosProgramadosClienteResponse_QNAME, ConsultarPagosProgramadosClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarNuevoPagoProgramado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pagos.javeriana.edu.co/", name = "eliminarNuevoPagoProgramado")
    public JAXBElement<EliminarNuevoPagoProgramado> createEliminarNuevoPagoProgramado(EliminarNuevoPagoProgramado value) {
        return new JAXBElement<EliminarNuevoPagoProgramado>(_EliminarNuevoPagoProgramado_QNAME, EliminarNuevoPagoProgramado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarNuevoPagoProgramadoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pagos.javeriana.edu.co/", name = "eliminarNuevoPagoProgramadoResponse")
    public JAXBElement<EliminarNuevoPagoProgramadoResponse> createEliminarNuevoPagoProgramadoResponse(EliminarNuevoPagoProgramadoResponse value) {
        return new JAXBElement<EliminarNuevoPagoProgramadoResponse>(_EliminarNuevoPagoProgramadoResponse_QNAME, EliminarNuevoPagoProgramadoResponse.class, null, value);
    }

}
