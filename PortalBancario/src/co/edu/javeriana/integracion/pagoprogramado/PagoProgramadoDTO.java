
package co.edu.javeriana.integracion.pagoprogramado;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for pagoProgramadoDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pagoProgramadoDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="convenioFactura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cuentaDebitar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="empresaFactura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaEjecucuion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="idPago" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="identificacionPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="identificadorCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numeroFactura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="periodicidad" type="{http://pagos.javeriana.edu.co/}tipoPeriodicidad" minOccurs="0"/&gt;
 *         &lt;element name="valorFactura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pagoProgramadoDTO", propOrder = {
    "convenioFactura",
    "cuentaDebitar",
    "empresaFactura",
    "fechaEjecucuion",
    "idPago",
    "identificacionPago",
    "identificadorCliente",
    "numeroFactura",
    "periodicidad",
    "valorFactura"
})
public class PagoProgramadoDTO {

    protected String convenioFactura;
    protected String cuentaDebitar;
    protected String empresaFactura;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaEjecucuion;
    protected Integer idPago;
    protected String identificacionPago;
    protected String identificadorCliente;
    protected String numeroFactura;
    @XmlSchemaType(name = "string")
    protected TipoPeriodicidad periodicidad;
    protected String valorFactura;

    /**
     * Gets the value of the convenioFactura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConvenioFactura() {
        return convenioFactura;
    }

    /**
     * Sets the value of the convenioFactura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConvenioFactura(String value) {
        this.convenioFactura = value;
    }

    /**
     * Gets the value of the cuentaDebitar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaDebitar() {
        return cuentaDebitar;
    }

    /**
     * Sets the value of the cuentaDebitar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaDebitar(String value) {
        this.cuentaDebitar = value;
    }

    /**
     * Gets the value of the empresaFactura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresaFactura() {
        return empresaFactura;
    }

    /**
     * Sets the value of the empresaFactura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresaFactura(String value) {
        this.empresaFactura = value;
    }

    /**
     * Gets the value of the fechaEjecucuion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaEjecucuion() {
        return fechaEjecucuion;
    }

    /**
     * Sets the value of the fechaEjecucuion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaEjecucuion(XMLGregorianCalendar value) {
        this.fechaEjecucuion = value;
    }

    /**
     * Gets the value of the idPago property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdPago() {
        return idPago;
    }

    /**
     * Sets the value of the idPago property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdPago(Integer value) {
        this.idPago = value;
    }

    /**
     * Gets the value of the identificacionPago property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificacionPago() {
        return identificacionPago;
    }

    /**
     * Sets the value of the identificacionPago property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificacionPago(String value) {
        this.identificacionPago = value;
    }

    /**
     * Gets the value of the identificadorCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificadorCliente() {
        return identificadorCliente;
    }

    /**
     * Sets the value of the identificadorCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificadorCliente(String value) {
        this.identificadorCliente = value;
    }

    /**
     * Gets the value of the numeroFactura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroFactura() {
        return numeroFactura;
    }

    /**
     * Sets the value of the numeroFactura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroFactura(String value) {
        this.numeroFactura = value;
    }

    /**
     * Gets the value of the periodicidad property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPeriodicidad }
     *     
     */
    public TipoPeriodicidad getPeriodicidad() {
        return periodicidad;
    }

    /**
     * Sets the value of the periodicidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPeriodicidad }
     *     
     */
    public void setPeriodicidad(TipoPeriodicidad value) {
        this.periodicidad = value;
    }

    /**
     * Gets the value of the valorFactura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorFactura() {
        return valorFactura;
    }

    /**
     * Sets the value of the valorFactura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorFactura(String value) {
        this.valorFactura = value;
    }

}
