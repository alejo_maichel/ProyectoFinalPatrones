package co.edu.javeriana.integracion.pagoprogramado.clientsample;

import co.edu.javeriana.integracion.pagoprogramado.*;

public class ClientSample {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        WSPagosProgramadosService service1 = new WSPagosProgramadosService();
	        System.out.println("Create Web Service...");
	        WSPagosProgramados port1 = service1.getWSPagosProgramadosPort();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port1.agregarNuevoPagoProgramado(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.consultarPagosProgramadosCRON(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.consultarPagosProgramadosCliente(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.eliminarNuevoPagoProgramado(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Create Web Service...");
	        WSPagosProgramados port2 = service1.getWSPagosProgramadosPort();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port2.agregarNuevoPagoProgramado(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.consultarPagosProgramadosCRON(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.consultarPagosProgramadosCliente(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.eliminarNuevoPagoProgramado(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
