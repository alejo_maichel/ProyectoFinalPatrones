
package co.edu.javeriana.integracion.pagoprogramado;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoPeriodicidad.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoPeriodicidad"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="MENSUAL"/&gt;
 *     &lt;enumeration value="TRIMESTRAL"/&gt;
 *     &lt;enumeration value="SEMESTRAL"/&gt;
 *     &lt;enumeration value="ANUAL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "tipoPeriodicidad")
@XmlEnum
public enum TipoPeriodicidad {

    MENSUAL,
    TRIMESTRAL,
    SEMESTRAL,
    ANUAL;

    public String value() {
        return name();
    }

    public static TipoPeriodicidad fromValue(String v) {
        return valueOf(v);
    }

}
