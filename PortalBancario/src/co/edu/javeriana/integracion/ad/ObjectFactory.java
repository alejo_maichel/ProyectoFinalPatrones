
package co.edu.javeriana.integracion.ad;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.edu.javeriana.integracion.ad package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultarIdentificacion_QNAME = new QName("http://ad.javeriana.edu.co/", "ConsultarIdentificacion");
    private final static QName _ConsultarIdentificacionResponse_QNAME = new QName("http://ad.javeriana.edu.co/", "ConsultarIdentificacionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.edu.javeriana.integracion.ad
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultarIdentificacion }
     * 
     */
    public ConsultarIdentificacion createConsultarIdentificacion() {
        return new ConsultarIdentificacion();
    }

    /**
     * Create an instance of {@link ConsultarIdentificacionResponse }
     * 
     */
    public ConsultarIdentificacionResponse createConsultarIdentificacionResponse() {
        return new ConsultarIdentificacionResponse();
    }

    /**
     * Create an instance of {@link Autenticacion }
     * 
     */
    public Autenticacion createAutenticacion() {
        return new Autenticacion();
    }

    /**
     * Create an instance of {@link Usuario }
     * 
     */
    public Usuario createUsuario() {
        return new Usuario();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarIdentificacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ad.javeriana.edu.co/", name = "ConsultarIdentificacion")
    public JAXBElement<ConsultarIdentificacion> createConsultarIdentificacion(ConsultarIdentificacion value) {
        return new JAXBElement<ConsultarIdentificacion>(_ConsultarIdentificacion_QNAME, ConsultarIdentificacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarIdentificacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ad.javeriana.edu.co/", name = "ConsultarIdentificacionResponse")
    public JAXBElement<ConsultarIdentificacionResponse> createConsultarIdentificacionResponse(ConsultarIdentificacionResponse value) {
        return new JAXBElement<ConsultarIdentificacionResponse>(_ConsultarIdentificacionResponse_QNAME, ConsultarIdentificacionResponse.class, null, value);
    }

}
