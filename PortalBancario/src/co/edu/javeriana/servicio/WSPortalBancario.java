package co.edu.javeriana.servicio;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import co.edu.javeriana.integracion.Integracion;
import co.edu.javeriana.integracion.pagoprogramado.PagoProgramadoDTO;

@WebService
public class WSPortalBancario {
	
	@WebMethod
	public Integer EjecutarPagosProgramados(Date fechaConsulta){
		Integer CantidadCobros = 0;
		List<PagoProgramadoDTO> listaPagos = new Integracion().ConsultarPagosProgramados(fechaConsulta);
		for (PagoProgramadoDTO dto : listaPagos) {
			if (new Integracion().debitarCuenta(dto.getCuentaDebitar(),Integer.parseInt(dto.getValorFactura()))){
				new Integracion().gatewayPasarela(dto.getConvenioFactura());
				CantidadCobros++;
			}
		}
		return CantidadCobros;
	}

}
