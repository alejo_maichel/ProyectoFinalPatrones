package co.edu.javeriana.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import co.edu.javeriana.integracion.Integracion;
import co.edu.javeriana.integracion.mainframe.CuentaDTO;
import co.edu.javeriana.integracion.prestamos.PrestamoDTO;

@ManagedBean
@ViewScoped
public class Dashboard {

	@ManagedProperty(value = "#{login}")
	private Login login;

	private List<CuentaDTO> cuentasCliente;

	private List<PrestamoDTO> prestamosCliente;

	private List<String> cobrosProgramados;

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public List<CuentaDTO> getCuentasCliente() {
		return cuentasCliente;
	}

	public void setCuentasCliente(List<CuentaDTO> cuentasCliente) {
		this.cuentasCliente = cuentasCliente;
	}

	public List<PrestamoDTO> getPrestamosCliente() {
		return prestamosCliente;
	}

	public void setPrestamosCliente(List<PrestamoDTO> prestamosCliente) {
		this.prestamosCliente = prestamosCliente;
	}

	public List<String> getCobrosProgramados() {
		return cobrosProgramados;
	}

	public void setCobrosProgramados(List<String> cobrosProgramados) {
		this.cobrosProgramados = cobrosProgramados;
	}

	@PostConstruct
	private void initDashBoard() {
		cuentasCliente = new Integracion().ConsultaCuentas(login.getUsuarioAutenticacion().getId());
		prestamosCliente = new Integracion().ConsultarPrestamos(login.getUsuarioAutenticacion().getId());
	}

}
