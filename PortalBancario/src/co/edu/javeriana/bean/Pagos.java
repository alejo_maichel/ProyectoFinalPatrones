package co.edu.javeriana.bean;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import co.edu.javeriana.integracion.Integracion;
import co.edu.javeriana.integracion.mainframe.CuentaDTO;
import co.edu.javeriana.integracion.pagoprogramado.PagoProgramadoDTO;

@ManagedBean
@ViewScoped
public class Pagos {

	@ManagedProperty(value = "#{login}")
	private Login login;

	private List<PagoProgramadoDTO> listaPagosClientes;

	private List<CuentaDTO> cuentasCliente;

	private PagoProgramadoDTO nuevoPago;
	private Date fechaNuevoPago;

	
	public Date getFechaNuevoPago() {
		return fechaNuevoPago;
	}

	public void setFechaNuevoPago(Date fechaNuevoPago) {
		this.fechaNuevoPago = fechaNuevoPago;
	}

	public List<CuentaDTO> getCuentasCliente() {
		return cuentasCliente;
	}

	public void setCuentasCliente(List<CuentaDTO> cuentasCliente) {
		this.cuentasCliente = cuentasCliente;
	}

	public PagoProgramadoDTO getNuevoPago() {
		return nuevoPago;
	}

	public void setNuevoPago(PagoProgramadoDTO nuevoPago) {
		this.nuevoPago = nuevoPago;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public List<PagoProgramadoDTO> getListaPagosClientes() {
		return listaPagosClientes;
	}

	public void setListaPagosClientes(List<PagoProgramadoDTO> listaPagosClientes) {
		this.listaPagosClientes = listaPagosClientes;
	}

	@PostConstruct
	public void initPagos() {
		nuevoPago = new PagoProgramadoDTO();
		listaPagosClientes = new Integracion().ConsultarPagosProgramados(login.getUsuarioAutenticacion().getId());
		cuentasCliente = new Integracion().ConsultaCuentas(login.getUsuarioAutenticacion().getId());
	}

	public void agregarNuevoPago() {
		nuevoPago.setIdPago(0);
		nuevoPago.setFechaEjecucuion(getXmlGregorianCalendarFromDate(fechaNuevoPago));
		nuevoPago.setIdentificadorCliente(login.getUsuarioAutenticacion().getId());
		new Integracion().AgregarPagosProgramados(nuevoPago);
		listaPagosClientes = new Integracion().ConsultarPagosProgramados(login.getUsuarioAutenticacion().getId());
		
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", "Se ha agregado un nuevo pago programado"));
	}
	
	public void eliminarPagoProgramado(PagoProgramadoDTO dto) {
		new Integracion().EliminarPagosProgramados(dto);
		listaPagosClientes = new Integracion().ConsultarPagosProgramados(login.getUsuarioAutenticacion().getId());
		
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", "Se ha eliminado un pago programado"));
	}
	
	public void pagarManualmente(PagoProgramadoDTO dto){
		new Integracion().debitarCuenta(dto.getCuentaDebitar(),Integer.parseInt(dto.getValorFactura()));
		new Integracion().gatewayPasarela(dto.getConvenioFactura());
		
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", "Se realizo el pago manual"));
	}

	public static XMLGregorianCalendar getXmlGregorianCalendarFromDate(final Date date) {
		try {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}
}
