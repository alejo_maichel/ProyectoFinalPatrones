package co.edu.javeriana.bean;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import co.edu.javeriana.integracion.Integracion;
import co.edu.javeriana.integracion.ad.Usuario;
import co.edu.javeriana.integracion.crm.Cliente;
import co.edu.javeriana.integracion.mainframe.CuentaDTO;
import co.edu.javeriana.integracion.prestamos.PrestamoDTO;


@ManagedBean
@SessionScoped
public class Login {

	private String usuario;
	private String contrasena;
	private Usuario usuarioAutenticacion;
	private Cliente clienteCRM;

	private Integer cuentaSeleccionada;
	private Integer prestarmoSeleccionado;

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuarioAutenticacion() {
		return usuarioAutenticacion;
	}

	public void setUsuarioAutenticacion(Usuario usuarioAutenticacion) {
		this.usuarioAutenticacion = usuarioAutenticacion;
	}

	public Cliente getClienteCRM() {
		return clienteCRM;
	}

	public void setClienteCRM(Cliente clienteCRM) {
		this.clienteCRM = clienteCRM;
	}

	public Integer getCuentaSeleccionada() {
		return cuentaSeleccionada;
	}

	public void setCuentaSeleccionada(Integer cuentaSeleccionada) {
		this.cuentaSeleccionada = cuentaSeleccionada;
	}

	public Integer getPrestarmoSeleccionado() {
		return prestarmoSeleccionado;
	}

	public void setPrestarmoSeleccionado(Integer prestarmoSeleccionado) {
		this.prestarmoSeleccionado = prestarmoSeleccionado;
	}

	public String RealizarLogin() {
		if (usuario != null && !usuario.isEmpty()) {
			if (contrasena != null && !contrasena.isEmpty()) {
				usuarioAutenticacion = new Integracion().consultarDirectorioActivo(usuario, contrasena);
				if (usuarioAutenticacion != null) {
					clienteCRM = new Integracion().consultarClienteCRM(usuarioAutenticacion.getId());
					try {
						FacesContext.getCurrentInstance().getExternalContext().redirect("pages/Inicio.xhtml");
					} catch (IOException e) {
						e.printStackTrace();
					}
					return "Inicio";
				} else {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR", "No se ha podido autenticar en el sistema"));
				}
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "La contrasena no puede ser vacia"));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "El usuario no puede ser vacio"));
		}
		return null;
	}

	public void realizarLogout() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.invalidate();
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("Login.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void MovimientosCuenta(Integer idCuenta) {
		try {
			cuentaSeleccionada = idCuenta;
			FacesContext.getCurrentInstance().getExternalContext().redirect("Cuentas.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void MovimientosPrestamo(Integer idPrestamo) {
		try {
			prestarmoSeleccionado = idPrestamo;
			FacesContext.getCurrentInstance().getExternalContext().redirect("Creditos.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
