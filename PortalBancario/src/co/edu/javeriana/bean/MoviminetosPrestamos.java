package co.edu.javeriana.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import co.edu.javeriana.integracion.Integracion;
import co.edu.javeriana.integracion.mainframe.CuentaDTO;
import co.edu.javeriana.integracion.prestamos.PrestamoDTO;

@ManagedBean
public class MoviminetosPrestamos {

	@ManagedProperty(value = "#{login}")
	private Login login;

	private PrestamoDTO prestamoSeleccionado;

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public PrestamoDTO getPrestamoSeleccionado() {
		return prestamoSeleccionado;
	}

	public void setPrestamoSeleccionado(PrestamoDTO prestamoSeleccionado) {
		this.prestamoSeleccionado = prestamoSeleccionado;
	}

	@PostConstruct
	public void initMovimientosPrestamo() {
		prestamoSeleccionado = new Integracion().ConsultarMovimientosPrestamo(login.getPrestarmoSeleccionado());
	}

}
