package co.edu.javeriana.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import co.edu.javeriana.integracion.Integracion;
import co.edu.javeriana.integracion.mainframe.CuentaDTO;

@ManagedBean
public class MoviminetosCuentas {

	@ManagedProperty(value = "#{login}")
	private Login login;

	private CuentaDTO cuentaSeleccionada;

	public CuentaDTO getCuentaSeleccionada() {
		return cuentaSeleccionada;
	}

	public void setCuentaSeleccionada(CuentaDTO cuentaSeleccionada) {
		this.cuentaSeleccionada = cuentaSeleccionada;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	@PostConstruct
	public void initMovimientosCuenta() {
		System.out.println(login.getCuentaSeleccionada());
		cuentaSeleccionada = new Integracion().ConsultarMovimientosCuentas(login.getCuentaSeleccionada());
		System.out.println(cuentaSeleccionada.getIdCuenta());
	}

}
