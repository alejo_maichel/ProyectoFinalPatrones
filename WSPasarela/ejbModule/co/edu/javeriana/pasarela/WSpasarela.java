package co.edu.javeriana.pasarela;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

@Stateless
@WebService
public class WSpasarela {

	@WebMethod
	public boolean PagarCamaraCompensacion(){
		return true;
	}
	
	@WebMethod
	public boolean PagarSWIFT(){
		return true;
	}
	
	@WebMethod
	public boolean PagarCasaCumpliminetoManual(){
		return true;
	}
	
	@WebMethod
	public boolean PagarCuentaCuenta(){
		return true;
	}
}
