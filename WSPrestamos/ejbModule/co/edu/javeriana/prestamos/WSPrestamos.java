package co.edu.javeriana.prestamos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import co.edu.javeriana.dto.MovimientoDTO;
import co.edu.javeriana.dto.PrestamoDTO;
import co.edu.javeriana.model.PagosRealizado;
import co.edu.javeriana.model.Prestamo;

@Stateless
@WebService
public class WSPrestamos {

	@PersistenceContext
	private EntityManager em;

	@WebMethod
	public List<PrestamoDTO> consultarPrestamosCliente(String idCliente) {
		if (idCliente != null && idCliente.length() >= 6) {
			List<Prestamo> prestamoTemp = em.createNamedQuery("Prestamo.findPrestamosCliente").setParameter("idcliente", idCliente).getResultList();
			List<PrestamoDTO> prestamosRespuesta = castPrestamoToXML(prestamoTemp);
			return prestamosRespuesta;
		}
		return null;
	}
	
	@WebMethod 
	public PrestamoDTO consultarMoviminetosCuenta(Integer idCuenta){
		
		if (idCuenta != null) {
			List<Prestamo> cuentasTemp = em.createNamedQuery("Prestamo.findPrestamoCliente").setParameter("idprestamo", idCuenta).getResultList();
			List<PrestamoDTO> cuentasCliente = castPrestamoToXML(cuentasTemp);
			if(cuentasCliente.size()>0){
				PrestamoDTO dto = cuentasCliente.get(0);
				return dto;
			}
		}
		return null;
	}
	
	private List<PrestamoDTO> castPrestamoToXML(List<Prestamo> prestamosCliente) {
		
		List<PrestamoDTO> listaPrestamos = new ArrayList<PrestamoDTO>();
		for (Prestamo prestamo : prestamosCliente) {
			PrestamoDTO dto = new PrestamoDTO();
			dto.setCantidadCuotas(prestamo.getCantidadCuotas());
			dto.setCliente(prestamo.getCliente());
			dto.setConvenio(prestamo.getConvenio());
			dto.setIdPrestamo(prestamo.getIdPrestamo());
			dto.setValorCuota(prestamo.getValorCuota());
			dto.setValorPrestamo(prestamo.getValorPrestamo());
			
			List<MovimientoDTO> movimientos = new ArrayList<MovimientoDTO>();
			for (PagosRealizado pr: prestamo.getPagosRealizados()) {
				MovimientoDTO mov = new MovimientoDTO();
				mov.setFechaCobro(pr.getFechaCobro());
				mov.setIdPagosRealizados(pr.getIdPagosRealizados());
				mov.setValorRecaudado(pr.getValorRecaudado());
				movimientos.add(mov);
			}
			dto.setListaMovimientos(movimientos);
			listaPrestamos.add(dto);
		}
		return listaPrestamos;
	}

}
