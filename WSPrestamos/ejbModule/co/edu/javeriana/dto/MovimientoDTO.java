package co.edu.javeriana.dto;

import java.util.Date;

public class MovimientoDTO {

	private int idPagosRealizados;

	private String fechaCobro;

	private String valorRecaudado;

	public int getIdPagosRealizados() {
		return idPagosRealizados;
	}

	public void setIdPagosRealizados(int idPagosRealizados) {
		this.idPagosRealizados = idPagosRealizados;
	}

	public String getFechaCobro() {
		return fechaCobro;
	}

	public void setFechaCobro(String fechaCobro) {
		this.fechaCobro = fechaCobro;
	}

	public String getValorRecaudado() {
		return valorRecaudado;
	}

	public void setValorRecaudado(String valorRecaudado) {
		this.valorRecaudado = valorRecaudado;
	}

}
