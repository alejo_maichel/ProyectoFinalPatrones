package co.edu.javeriana.dto;

import java.util.List;

public class PrestamoDTO {

	private int idPrestamo;

	private int cantidadCuotas;

	private String convenio;

	private String valorCuota;

	private String valorPrestamo;

	private String cliente;

	private List<MovimientoDTO> listaMovimientos;

	public int getIdPrestamo() {
		return idPrestamo;
	}

	public void setIdPrestamo(int idPrestamo) {
		this.idPrestamo = idPrestamo;
	}

	public int getCantidadCuotas() {
		return cantidadCuotas;
	}

	public void setCantidadCuotas(int cantidadCuotas) {
		this.cantidadCuotas = cantidadCuotas;
	}

	public String getConvenio() {
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}

	public String getValorCuota() {
		return valorCuota;
	}

	public void setValorCuota(String valorCuota) {
		this.valorCuota = valorCuota;
	}

	public String getValorPrestamo() {
		return valorPrestamo;
	}

	public void setValorPrestamo(String valorPrestamo) {
		this.valorPrestamo = valorPrestamo;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public List<MovimientoDTO> getListaMovimientos() {
		return listaMovimientos;
	}

	public void setListaMovimientos(List<MovimientoDTO> listaMovimientos) {
		this.listaMovimientos = listaMovimientos;
	}

}
