package co.edu.javeriana.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PagosRealizados database table.
 * 
 */
@Entity
@Table(name="PagosRealizados")
@NamedQuery(name="PagosRealizado.findAll", query="SELECT p FROM PagosRealizado p")
public class PagosRealizado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPagosRealizados;

	private String fechaCobro;

	private String valorRecaudado;

	//bi-directional many-to-one association to Prestamo
	@ManyToOne
	@JoinColumn(name="idPrestamo")
	private Prestamo prestamo;

	public PagosRealizado() {
	}

	public int getIdPagosRealizados() {
		return this.idPagosRealizados;
	}

	public void setIdPagosRealizados(int idPagosRealizados) {
		this.idPagosRealizados = idPagosRealizados;
	}

	public String getFechaCobro() {
		return this.fechaCobro;
	}

	public void setFechaCobro(String fechaCobro) {
		this.fechaCobro = fechaCobro;
	}

	public String getValorRecaudado() {
		return this.valorRecaudado;
	}

	public void setValorRecaudado(String valorRecaudado) {
		this.valorRecaudado = valorRecaudado;
	}

	public Prestamo getPrestamo() {
		return this.prestamo;
	}

	public void setPrestamo(Prestamo prestamo) {
		this.prestamo = prestamo;
	}

}