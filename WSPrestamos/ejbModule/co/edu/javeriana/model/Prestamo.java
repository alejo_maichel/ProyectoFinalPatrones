package co.edu.javeriana.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Prestamo database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Prestamo.findAll", query="SELECT p FROM Prestamo p"),
	@NamedQuery(name="Prestamo.findPrestamosCliente", query="SELECT p FROM Prestamo p where p.cliente = :idcliente"),
	@NamedQuery(name="Prestamo.findPrestamoCliente", query="SELECT p FROM Prestamo p where p.idPrestamo = :idprestamo")
})
public class Prestamo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPrestamo;

	private int cantidadCuotas;

	private String convenio;

	@Lob
	private String valorCuota;

	@Lob
	private String valorPrestamo;
	
	private String cliente;

	//bi-directional many-to-one association to PagosRealizado
	@OneToMany(mappedBy="prestamo")
	private List<PagosRealizado> pagosRealizados;

	public Prestamo() {
	}

	public int getIdPrestamo() {
		return this.idPrestamo;
	}

	public void setIdPrestamo(int idPrestamo) {
		this.idPrestamo = idPrestamo;
	}

	public int getCantidadCuotas() {
		return this.cantidadCuotas;
	}

	public void setCantidadCuotas(int cantidadCuotas) {
		this.cantidadCuotas = cantidadCuotas;
	}

	public String getConvenio() {
		return this.convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}

	public String getValorCuota() {
		return this.valorCuota;
	}

	public void setValorCuota(String valorCuota) {
		this.valorCuota = valorCuota;
	}

	public String getValorPrestamo() {
		return this.valorPrestamo;
	}

	public void setValorPrestamo(String valorPrestamo) {
		this.valorPrestamo = valorPrestamo;
	}

	public List<PagosRealizado> getPagosRealizados() {
		return this.pagosRealizados;
	}

	public void setPagosRealizados(List<PagosRealizado> pagosRealizados) {
		this.pagosRealizados = pagosRealizados;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public PagosRealizado addPagosRealizado(PagosRealizado pagosRealizado) {
		getPagosRealizados().add(pagosRealizado);
		pagosRealizado.setPrestamo(this);

		return pagosRealizado;
	}

	public PagosRealizado removePagosRealizado(PagosRealizado pagosRealizado) {
		getPagosRealizados().remove(pagosRealizado);
		pagosRealizado.setPrestamo(null);

		return pagosRealizado;
	}

}